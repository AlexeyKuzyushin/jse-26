package ru.rencredit.jschool.kuzyushin.tm.exception.user;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class AccessDeniedException  extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }
}
