package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskViewByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-view-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        if (serviceLocator != null) {
            @Nullable final String name = TerminalUtil.nextLine();
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            @Nullable final TaskDTO taskDTO = serviceLocator.getTaskEndpoint().findTaskByName(sessionDTO, name);
            if (taskDTO == null) return;
            System.out.println("ID: " + taskDTO.getId());
            System.out.println("NAME: " + taskDTO.getName());
            System.out.println("DESCRIPTION: " + taskDTO.getDescription());
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
