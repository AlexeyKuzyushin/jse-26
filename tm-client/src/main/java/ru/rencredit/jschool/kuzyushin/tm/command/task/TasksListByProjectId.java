package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

import java.util.List;

public class TasksListByProjectId extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list-by-projectId";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list of project";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        System.out.println("ENTER PROJECTID:");
        if (serviceLocator != null) {
            @Nullable final String projectId = TerminalUtil.nextLine();
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            @Nullable final List<TaskDTO> tasksDTO =
                    serviceLocator.getTaskEndpoint().findAllTasksByProjectId(sessionDTO, projectId);
            int index = 1;
            for (TaskDTO taskDTO: tasksDTO) {
                System.out.println(index + ". " + taskDTO.getName());
                index++;
            }
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
