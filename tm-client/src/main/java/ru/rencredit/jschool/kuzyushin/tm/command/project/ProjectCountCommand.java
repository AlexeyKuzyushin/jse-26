package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

public class ProjectCountCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-count";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Count all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[COUNT PROJECTS]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("COUNT: " + serviceLocator.getProjectEndpoint().countAllProjects(sessionDTO));
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
