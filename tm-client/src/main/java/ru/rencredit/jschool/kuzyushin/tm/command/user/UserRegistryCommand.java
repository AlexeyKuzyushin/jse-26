package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-mail:");
        @Nullable final String email = TerminalUtil.nextLine();
        if (serviceLocator != null) {
            serviceLocator.getAdminUserEndpoint().createUserWithEmail(login, password, email);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
