package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    Long count();

    @NotNull
    List<User> findAll();

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);
}
