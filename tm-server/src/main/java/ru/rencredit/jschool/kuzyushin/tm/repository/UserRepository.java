package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM User e", Long.class).getSingleResult();
    }

    @Override
    public @NotNull List<User> findAll() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @Nullable
    @Override
    public User findById(final @NotNull String id) {
        return entityManager.createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public User findByLogin(final @NotNull String login) {
        return entityManager.createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public void removeById(final @NotNull String id) {
        @Nullable final User user = findById(id);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public void removeByLogin(final @NotNull String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        entityManager.remove(user);
    }
}
