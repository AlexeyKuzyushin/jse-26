package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;

public interface IDataEndpoint {

    void saveDataBinary(@Nullable SessionDTO sessionDTO) throws Exception;

    void saveDataJson(@Nullable SessionDTO sessionDTO) throws Exception;

    void saveDataXml(@Nullable SessionDTO sessionDTO) throws Exception;

    void loadDataBinary(@Nullable SessionDTO sessionDTO) throws Exception;

    void loadDataJson(@Nullable SessionDTO sessionDTO) throws Exception;

    void loadDataXml(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearDataBinary(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearDataJson(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearDataXml(@Nullable SessionDTO sessionDTO) throws Exception;
}
